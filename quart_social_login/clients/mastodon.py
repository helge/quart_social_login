import aiohttp
import logging
import json
from urllib.parse import urlencode

logger = logging.getLogger(__name__)


class MastodonClient:
    """Encapsulate the basic dynamic of creating a Mastodon client
    application. Building an authorization link, obtaining the access
    token and retrieving the user"""

    def __init__(
        self,
        domain,
        client_id=None,
        client_secret=None,
        session=None,
        redirect_uri="urn:ietf:wg:oauth:2.0:oob",
        scopes="read",
    ):
        self.domain = domain
        self.client_id = client_id
        self.client_secret = client_secret
        self.session = session
        self.redirect_uri = redirect_uri
        self.scopes = scopes

    async def init(self):
        if not self.client_id:
            if not self.session:
                self.session = aiohttp.ClientSession()

            response = await self.session.post(
                f"https://{self.domain}/api/v1/apps",
                data={
                    "client_name": "quart social login",
                    "redirect_uris": self.redirect_uri,
                    "scopes": self.scopes,
                },
            )

            response.raise_for_status()

            data = await response.json()

            logger.info(json.dumps(data))

            self.client_id = data["client_id"]
            self.client_secret = data["client_secret"]

    def build_authorize_link(self):
        """Creates the link to request authorization from mastodon"""

        return f"https://{self.domain}/oauth/authorize?" + urlencode(
            {
                "response_type": "code",
                "client_id": self.client_id,
                "redirect_uri": self.redirect_uri,
                "scope": self.scopes,
            }
        )

    async def obtain_token(self, code):
        if not self.session:
            self.session = aiohttp.ClientSession()

        response = await self.session.post(
            f"https://{self.domain}/oauth/token",
            data={
                "grant_type": "authorization_code",
                "code": code,
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "redirect_uri": self.redirect_uri,
            },
        )

        data = await response.json()

        self.access_token = data["access_token"]

    async def account(self):
        """Retrieves the account that authorized this client"""
        if not self.session:
            self.session = aiohttp.ClientSession()

        response = await self.session.get(
            f"https://{self.domain}/api/v1/accounts/verify_credentials",
            headers={"Authorization": f"Bearer {self.access_token}"},
        )

        data = await response.json()

        return data["username"] + "@" + self.domain
