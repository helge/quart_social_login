import aiohttp
from tortoise import Tortoise

from quart_social_login.models import MastododonAppCredentials
from .mastodon import MastodonClient


class Database:
    def __init__(self, db_url="sqlite://quart_social_login.sqlite3"):
        self.db_url = db_url

    async def __aenter__(self):
        await Tortoise.init(
            db_url=self.db_url,
            modules={"quart_social_login": ["quart_social_login.models"]},
        )
        await Tortoise.generate_schemas()

        return self

    async def __aexit__(self, *args):
        await Tortoise.close_connections()


async def mastodon_client_for_domain(
    domain: str,
    redirect_uri: str = "urn:ietf:wg:oauth:2.0:oob",
    session: aiohttp.ClientSession = None,
    scopes: str = "read",
) -> MastodonClient:
    """Supplies a MastodonClient given an url, credentials
    are either looked up in the database or obtained by
    querying the remote server"""
    if not session:
        session = aiohttp.ClientSession()

    credentials, created = await MastododonAppCredentials.get_or_create(domain=domain)

    client = MastodonClient(
        domain,
        session=session,
        client_id=credentials.client_id,
        client_secret=credentials.client_secret,
        redirect_uri=redirect_uri,
        scopes=scopes,
    )

    if credentials.client_id is not None and credentials.client_secret is not None:
        return client

    await client.init()

    credentials.client_id = client.client_id
    credentials.client_secret = client.client_secret
    await credentials.save()

    return client
