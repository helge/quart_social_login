from . import Database, mastodon_client_for_domain


async def test_with_mastodon_client():
    async with Database():
        client = await mastodon_client_for_domain("mastodon.social")

        link = client.build_authorize_link()

        assert link.startswith("https://mastodon.social")

        await client.session.close()
