from enum import Enum
import logging

from quart_auth import AuthUser
from .models import MastodonAccessToken

logger = logging.getLogger(__name__)


class SocialAuthType(Enum):
    """Enumeration values MASTODON and HELLO"""

    MASTODON = "MASTODON"
    HELLO = "HELLO"


class SocialAuthUser(AuthUser):
    """Represents a user that successfully authenticated

    auth_type is the type and identifier the corresponding
    identifier. The usage of auth_id is necessary to be compatible
    with quart_auth."""

    def __init__(
        self,
        auth_id: str | None = None,
        auth_type: SocialAuthType | None = None,
        identifier: str | None = None,
    ):
        if auth_id:
            auth_type_tmp, identifier = auth_id.split("_", 1)
            auth_type = SocialAuthType(auth_type_tmp)

        if auth_type is None:
            super().__init__(None)
            return

        self.auth_type = auth_type
        self.identifier = identifier

        auth_id = auth_type.value + "_" + identifier

        super().__init__(auth_id)

    async def access_token(self) -> str | None:
        """Retrieves the mastodon access token of authorized user"""

        result = await MastodonAccessToken.get_or_none(auth_id=self.auth_id)
        if result:
            return result.access_token

        return None

    @staticmethod
    def from_mastodon(account):
        return SocialAuthUser(auth_type=SocialAuthType.MASTODON, identifier=account)

    @staticmethod
    def from_hello(sub):
        return SocialAuthUser(auth_type=SocialAuthType.HELLO, identifier=sub)
