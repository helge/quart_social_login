import aiohttp
import secrets

from tortoise.contrib.quart import register_tortoise
from quart_auth import QuartAuth

from .hello_auth import hello_auth
from .mastodon_auth import mastodon_auth
from .types import SocialAuthUser


def QuartSocialLogin(
    app,
    hello_client_id=None,
    db_url="sqlite://quart_social_login.sqlite3",
    enable_mastodon: bool = True,
    store_mastodon_access_token: bool = False,
    mastodon_scopes: str = "read",
):
    """Initializes quart social login. Provides
    /mastodon_auth/login?domain=mastodon_domain and /hello_auth/login
    paths. On successful login redirected to "/". The
    quart_auth.current_user variable will be of type SocialAuthUser

    Ensures app.config["session"] is an aiohttp.ClientSession

    :param app: The quart app
    :param hello_client_id: The client id from hello.coop (a uuid4)
    :param db_url: Database to store mastodon app credentials in
    :param enable_mastodon: set to False to not initialize mastodon_auth
    :param store_mastoodn_access_token: set to True to store access tokens
    :param mastodon_scopes: list of scopes to use for mastodon
    """
    app.secret_key = secrets.token_urlsafe(32)
    QuartAuth.user_class = SocialAuthUser
    QuartAuth(app)

    if enable_mastodon:
        register_tortoise(
            app,
            db_url=db_url,
            modules={"quart_social_login": ["quart_social_login.models"]},
            generate_schemas=True,
        )
        app.register_blueprint(mastodon_auth, url_prefix="/mastodon_auth")
        app.config["quart_social_login"] = {
            "store_mastodon_access_token": store_mastodon_access_token,
            "mastodon_scopes": mastodon_scopes,
        }

    if hello_client_id:
        app.config["hello_client_id"] = hello_client_id
        app.config["nonce"] = secrets.token_urlsafe(32)
        app.register_blueprint(hello_auth, url_prefix="/hello_auth")

    @app.before_serving
    async def startup():
        if "session" not in app.config:
            app.config["session"] = aiohttp.ClientSession()
