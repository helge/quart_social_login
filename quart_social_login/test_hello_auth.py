import json
from unittest.mock import AsyncMock

import pytest
from quart import Quart
from quart_auth import QuartAuth

from .hello_auth import hello_auth


@pytest.fixture
async def test_app():
    app = Quart(__name__)

    app.secret_key = "secret"

    QuartAuth(app)
    app.register_blueprint(hello_auth)

    app.config["hello_client_id"] = "client_id"
    app.config["nonce"] = "nonce"

    yield app


async def test_hello_auth_get(test_app):
    client = test_app.test_client()

    response = await client.get("/login")

    assert response.status_code == 302

    assert (
        response.headers["location"]
        == "https://wallet.hello.coop/authorize?client_id=client_id&nonce=nonce&redirect_uri=http%3A%2F%2Flocalhost%2Flogin&response_mode=form_post&response_type=id_token&scope=openid+email+nickname"  # noqa F501
    )


async def test_hello_auth_post(test_app):
    session = AsyncMock()
    response = AsyncMock()
    response.text.return_value = json.dumps(
        {"sub": "sup", "email": "bob@example.com", "nickname": "alice"}
    )
    session.post.return_value = response
    test_app.config["session"] = session
    client = test_app.test_client()

    response = await client.post("/login", form={"id_token": "123"})

    assert response.status_code == 302

    assert response.headers["location"] == "/"
