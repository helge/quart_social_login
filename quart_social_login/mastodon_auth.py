from quart import Blueprint, request, current_app, redirect
from quart_auth import login_user
from .clients import mastodon_client_for_domain
from .types import SocialAuthUser
from .models import MastodonAccessToken

mastodon_auth = Blueprint("mastodon_auth", __name__)


@mastodon_auth.get("/login")
async def mastodon_login():
    domain = request.args.get("domain")

    if domain is None:
        return "Please provide a domain", 400

    redirect_uri = request.url.split("?")[0] + "/" + domain

    client = await mastodon_client_for_domain(
        domain,
        redirect_uri=redirect_uri,
        session=current_app.config["session"],
        scopes=current_app.config["quart_social_login"]["mastodon_scopes"],
    )

    return redirect(client.build_authorize_link())


@mastodon_auth.get("/login/<domain>")
async def mastodon_account(domain):
    code = request.args.get("code")

    redirect_uri = request.url.split("?")[0]

    client = await mastodon_client_for_domain(
        domain,
        redirect_uri=redirect_uri,
        session=current_app.config["session"],
        scopes=current_app.config["quart_social_login"]["mastodon_scopes"],
    )
    await client.obtain_token(code)
    account = await client.account()
    auth_user = SocialAuthUser.from_mastodon(account)
    login_user(auth_user)

    if current_app.config["quart_social_login"]["store_mastodon_access_token"]:
        await MastodonAccessToken.update_or_create(
            auth_id=auth_user.auth_id, defaults={"access_token": client.access_token}
        )

    return redirect("/")
