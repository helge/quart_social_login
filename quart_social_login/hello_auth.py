import json
import logging
from urllib.parse import urlencode

from quart import Blueprint, current_app, redirect, request
from quart_auth import login_user

from .types import SocialAuthUser

logger = logging.getLogger(__name__)

hello_auth = Blueprint("hello_auth", __name__)


@hello_auth.get("/login")
async def hello_login():
    client_id = current_app.config["hello_client_id"]
    nonce = current_app.config["nonce"]

    redirect_uri = request.url
    if request.headers.get("X-Forwarded-Proto") == "https":
        redirect_uri = redirect_uri.replace("http://", "https://")

    logger.info(redirect_uri)

    url_to_open = "https://wallet.hello.coop/authorize?" + urlencode(
        {
            "client_id": client_id,
            "nonce": nonce,
            "redirect_uri": redirect_uri,
            "response_mode": "form_post",
            "response_type": "id_token",
            "scope": "openid+email+nickname",
        }
    ).replace("%2B", "+")

    return redirect(url_to_open)


@hello_auth.post("/login")
async def hello_id_token():
    await request.get_data(parse_form_data=True)

    form_data = await request.form

    id_token = form_data["id_token"]
    client_id = current_app.config["hello_client_id"]
    nonce = current_app.config["nonce"]

    session = current_app.config["session"]
    validation_result = await session.post(
        "https://wallet.hello.coop/oauth/introspect",
        data={"token": id_token, "client_id": client_id, "nonce": nonce},
    )

    validation_parsed = json.loads(await validation_result.text())

    try:
        sub = validation_parsed["sub"]
        logger.info("Signed in with %s", sub)

    except Exception as ex:
        logger.warning(validation_parsed)
        logger.warning(ex)
        return "Login in failed"

    login_user(SocialAuthUser.from_hello(sub))

    return redirect("/")
