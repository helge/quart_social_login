from tortoise.models import Model as TortoiseModel
from tortoise import fields


class Model(TortoiseModel):
    class Meta:
        model = "quart_social_login"


class MastododonAppCredentials(Model):
    domain = fields.CharField(max_length=255, primary_key=True)
    client_id = fields.CharField(max_length=255, null=True)
    client_secret = fields.CharField(max_length=255, null=True)


class MastodonAccessToken(Model):
    auth_id = fields.CharField(max_length=255, primary_key=True)
    access_token = fields.CharField(max_length=255)
