.. Quart Social Login documentation master file, created by
   sphinx-quickstart on Fri Jun 30 10:31:19 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Quart Social Login's documentation!
==============================================

This package can be installed via pip

.. code-block:: bash

   pip install quart_social_login


The most basic usage is given by

.. code-block:: python

   from quart import Quart
   from quart_social_login import QuartSocialLogin

   app = Quart(__name__)
   QuartSocialLogin(app, hello_client_id=HELLO_CLIENT_ID)



References
----------

The following modules form the public interface

.. automodule:: quart_social_login
	:members:
.. automodule:: quart_social_login.types
	:members:





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
