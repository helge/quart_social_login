import os
import sys

# from importlib.metadata import version as meta_version

sys.path.insert(0, os.path.abspath(".."))

project = "Quart Social Login"
copyright = "2023, Helge"
author = "Helge"
release = "0.1.0"


extensions = ["sphinx.ext.autodoc", "sphinx.ext.viewcode"]

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_context = {
    "source_url_prefix": "https://codeberg.org/helge/quart_social_login/src/branch/main/docs/",
}
