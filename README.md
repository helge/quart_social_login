# quart_social_auth

This quart extension should enable one to use various social login services to log into a service.

## Basic usage

```bash
pip install quart_social_auth
```

Then one is able to enable quart social login with

```python
from quart import Quart
from quart_social_login import QuartSocialLogin

app = Quart(__name__)
QuartSocialLogin(app, hello_client_id=HELLO_CLIENT_ID)
```

To obtain a hello client id, consult [hello](https://hello.coop).
