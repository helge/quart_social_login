import os
import logging
from quart import Quart

from quart_auth import current_user
from quart_social_login import QuartSocialLogin

logging.basicConfig(level=logging.INFO)

app = Quart(__name__)
QuartSocialLogin(
    app,
    hello_client_id=os.environ.get("HELLO_CLIENT_ID"),
    mastodon_scopes="read:accounts read:lists write:lists",
    store_mastodon_access_token=True,
)


@app.get("/")
async def main():
    user = current_user

    if await user.is_authenticated:
        return f"""<html>
    <head>
    <title>Quart Social Login Demonstration</title>
    </head>
    <body>
        {user.auth_type.value}<br/>{user.identifier}
    </body>
    </html>"""

    return """<html>
    <head>
    <title>Quart Social Login Demonstration</title>
    </head>
    <body>
    <form method="GET" action="/mastodon_auth/login">
    <input type="text" name="domain" value="mastodon.social" />
    <input type="submit" value="login with domain" />
    </form>
    <hr>
    <a href="/hello_auth/login">With Hello</a>
    </body>
    </html>"""


if __name__ == "__main__":
    app.run()
